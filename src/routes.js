import React from 'react';
import PropTypes from 'prop-types';
import { Route } from 'react-router-dom';
import { connect } from 'react-redux';
import withStyles from '@material-ui/core/styles/withStyles';

const styles = ({ breakpoints }) => ({
  container: {
    padding: '6em 0 0 0',
    [breakpoints.up('md')]: { padding: '24em 1em 1em 1em' },
    [breakpoints.up('lg')]: { padding: '18em 0 0 0' },
  },
  containerHome: {
    [breakpoints.up('sm')]: { padding: 0 },
  },
  search: {
    paddingTop: '5em',
  },
  containerFull: {
    maxWidth: 'fit-content',
    padding: '6em 0 0 0',
    [breakpoints.up('md')]: { padding: '21em 0 0 0' },
    [breakpoints.up('lg')]: { padding: '13em 0 0 0' },
  },
  containerHomeNoBanner: {
    padding: '10em 0 0 0',
    [breakpoints.up('md')]: { padding: '8em 0 0 0' },
  },
  containerNoBanner: {
    padding: '6.2em 0 0 0',
    [breakpoints.up('md')]: { padding: '16em 0 0 0' },
  },
});

const Routes = ({
  classes, isHome, fullwidth, hasBanner, ...rest
}) => {
  window.scrollTo(0, 0);

  if (fullwidth) {
    return (
      <>
        <Route {...rest} />
      </>
    );
  }

  return (
    <>
      <Route {...rest} />
    </>
  );
};

const mapStateToProps = ({ styleConfig }) => ({ hasBanner: styleConfig.data.hasBanner });

export default withStyles(styles)(connect(mapStateToProps)(Routes));

Routes.propTypes = {
  fullwidth: PropTypes.bool,
  isHome: PropTypes.bool,
  hasBanner: PropTypes.bool,
};

Routes.defaultProps = {
  fullwidth: false,
  hasBanner: null,
  isHome: null,
};

Routes.defaultProps = {};
