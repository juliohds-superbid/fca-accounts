import React from 'react';
import { I18nextProvider } from 'react-i18next';
import { persistStore } from 'redux-persist';
import { PersistGate } from 'redux-persist/integration/react';
import { Provider } from 'react-redux';
import { Switch, Route } from 'react-router-dom';
import { ConnectedRouter } from 'connected-react-router';
import store, { history } from './stores/configStore';
import i18n from './i18n';
import Layout from './theme/layout';

function App() {
  return (
    <Provider store={store}>
      <I18nextProvider i18n={i18n}>
        <PersistGate loading={null} persistor={persistStore(store)}>
          <ConnectedRouter history={history}>
            <Switch>
              <Route path="/" component={Layout} />
            </Switch>
          </ConnectedRouter>
        </PersistGate>
      </I18nextProvider>
    </Provider>
  );
}

export default App;
