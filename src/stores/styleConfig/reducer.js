import * as types from './types';

const initialState = {
  isLoading: false,
  data: {},
};

const styleConfig = (state = initialState, action) => {
  switch (action.type) {
    case types.STYLE_CONFIG_REQUEST:
      return {
        ...state,
        isLoading: true,
      };
    case types.STYLE_CONFIG_FULFILLED:
      return {
        ...state,
        isLoading: false,
        data: action.payload,
      };
    case types.STYLE_CONFIG_REJECT:
      return {
        ...state,
        isLoading: false,
      };
    default:
      return state;
  }
};

export default styleConfig;
