import { createAction } from 'redux-actions';
import * as types from './types';

export const styleConfigRequest = createAction(types.STYLE_CONFIG_REQUEST);
export const styleConfigFulfilled = createAction(types.STYLE_CONFIG_FULFILLED);
export const styleConfigReject = createAction(types.STYLE_CONFIG_REJECT);
