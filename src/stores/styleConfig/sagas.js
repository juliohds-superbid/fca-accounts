import {
  takeLatest, put, all, call,
} from 'redux-saga/effects';
import axios from 'axios';
import * as types from './types';
import { styleConfigFulfilled, styleConfigReject } from './actions';

const callApi = async (uri) => axios.get(uri);

function* styleConfigRequestSaga() {
  try {
    const { data, status } = yield call(callApi, './style.config.json');
    return yield status < 400 ? all([put(styleConfigFulfilled(data))]) : all([put(styleConfigReject())]);
  } catch (responseWithError) {
    return yield put(styleConfigReject());
  }
}

export default [takeLatest(types.STYLE_CONFIG_REQUEST, styleConfigRequestSaga)];
