import * as types from './types';

const initialState = {
  user: null,
  loading: false,
};

const user = (state = initialState, action) => {
  switch (action.type) {
    case types.USER_REQUEST:
      return {
        ...state,
        user: action.payload,
        loading: true,
      };

    case types.USER_REJECT:
      return {
        ...state,
        user: action.payload,
        loading: false,
      };

    case types.USER_FULFILLED:
      return {
        ...state,
        user: action.payload,
        loading: false,
      };

    default:
      return state;
  }
};

export default user;
