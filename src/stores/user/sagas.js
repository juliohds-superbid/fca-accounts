import { takeLatest, put } from 'redux-saga/effects';
import * as types from './types';

export function* getUser() {
  try {
    return yield put({
      type: types.USER_FULFILLED,
      user: { name: 'Julio' },
    });
  } catch (error) {
    return yield put({ type: types.USER_REJECT, error });
  }
}

export default [
  takeLatest(types.USER_REQUEST, getUser),
];
