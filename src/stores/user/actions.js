
import { createAction } from 'redux-actions';
import * as types from './types';

export const userRequest = createAction(types.USER_REQUEST);
export const userFulfilled = createAction(types.USER_FULFILLED);
export const userReject = createAction(types.USER_REJECT);
