
import { createAction } from 'redux-actions';
import * as types from './types';

export const loginOpenRegister = createAction(types.LOGIN_OPEN_REGISTER);
export const loginCloseRegister = createAction(types.LOGIN_CLOSE_REGISTER);

export const loginSetState = createAction(types.LOGIN_SET_STATE);
export const loginBackState = createAction(types.LOGIN_BACK_STATE);
