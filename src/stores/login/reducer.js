import * as types from './types';
import enumStateLogin from './utilsStoreLogin';

const initialState = {
  openRegister: false,
  stateLogin: enumStateLogin.INITIAL,
  loading: false,
};

const user = (state = initialState, action) => {
  switch (action.type) {
    case types.LOGIN_SET_STATE:
      return {
        ...state,
        stateLogin: action.payload,
      };
    case types.LOGIN_BACK_STATE:
      return {
        ...state,
        stateLogin: action.payload !== null ? action.payload : state.stateLogin - 1,
      };
    case types.LOGIN_OPEN_REGISTER:
      return {
        ...state,
        openRegister: true,
      };

    case types.LOGIN_CLOSE_REGISTER:
      return {
        ...state,
        openRegister: false,
      };

    default:
      return state;
  }
};

export default user;
