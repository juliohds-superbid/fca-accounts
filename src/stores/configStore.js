/* eslint-disable no-underscore-dangle */
import createSagaMiddleware from 'redux-saga';
import logger from 'redux-logger';
import { persistReducer } from 'redux-persist';
import { createStore, applyMiddleware, combineReducers } from 'redux';
import { createBrowserHistory } from 'history';
import { routerMiddleware, connectRouter } from 'connected-react-router';
import { composeWithDevTools } from 'redux-devtools-extension';
import storage from 'redux-persist/lib/storage';
import reducers from './reducers';
import sagas from './sagas';

export const history = createBrowserHistory();

const combReducers = combineReducers({ ...reducers, router: connectRouter(history) });

const createRootReducer = (state, action) => {
  if (action.type === 'REQUEST_LOGOUT') {
    Object.keys(state).forEach((key) => {
      storage.removeItem(`persist:${key}`);
    });
    state = undefined;
  }

  return combReducers(state, action);
};

const persistConfig = {
  key: 'root',
  storage,
  whitelist: ['', ''],
};

const persistedReducer = persistReducer(persistConfig, createRootReducer);

const routeMiddleware = routerMiddleware(history);
const sagaMiddleware = createSagaMiddleware();

const middlewares = process.env.LOGGER ? [routeMiddleware, sagaMiddleware, logger] : [routeMiddleware, sagaMiddleware];

const store = createStore(persistedReducer, composeWithDevTools(applyMiddleware(...middlewares)));
window.store = store;

sagaMiddleware.run(sagas);

export default store;
