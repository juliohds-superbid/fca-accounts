import { all } from 'redux-saga/effects';

import userSaga from './user/sagas';
import stlyeConfigSaga from './styleConfig/sagas';

function* rootSaga() {
  yield all(userSaga);
  yield all(stlyeConfigSaga);
}

export default rootSaga;
