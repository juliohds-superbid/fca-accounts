import { reducers } from './index';

const rootReducer = {
  ...reducers,
};

export default rootReducer;
