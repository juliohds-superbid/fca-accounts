const linksHeaderAdmin = [
  { name: 'Cadastro de usuários', link: '/admin/register', options: null },
  { name: 'Lista de usuários', link: '/admin/list', options: null },
];

export default linksHeaderAdmin;
