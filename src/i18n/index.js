import i18n from 'i18next';
import { initReactI18next } from 'react-i18next';
import { pt, en } from './locales';

const languages = ['pt', 'en'];

i18n.use(initReactI18next).init({
  resources: {
    en,
    pt,
  },
  lng: 'pt',
  fallbackLng: 'pt',
  interpolation: {
    escapeValue: false,
  },
});

const changeLanguage = (language) => {
  const languageValid = languages.includes(language) ? language : 'pt';
  localStorage.setItem('language', languageValid);
  i18n.changeLanguage(languageValid);
};

export default i18n;
export { changeLanguage };
