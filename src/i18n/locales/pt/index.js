export default {
  translation: {
    CANCEL: 'Cancelar',
    SEND: 'Enviar',
    LOCKAREA: 'Área restrita',
    CLOSE: 'Fechar',
    BACK: 'Voltar',
    SIGNIN: {
      WELLCOME_MAIN: 'Seja bem-vindo ao portal de vendas exclusivo FCA.',
      WELLCOME_DESCRIPTION: 'Faça seu login ou cadastre-se para ter acesso as ofertas:',
      FORGOT_PASSWORD_MAIN: 'Recuperação de senha',
      FORGOT_PASSWORD_DESCRIPTION: 'Digite seu e-mail da empresa para cadastrar uma nova senha:',
      FORGOT_SUCCESS_MAIN: 'E-mail enviado com sucesso!',
      FORGOT_SUCCESS_DESCRIPTION: 'Verifique sua caixa de entrada e clique no link.',
    },
    MODAL: {
      SIGNUP: {
        INTRODUCTION: 'Informe seu número de CPF:',
        EMAIL_SEND_TITLE: 'Confirmação enviada por e-mail.',
        EMAIL_SEND_DESCRIPTION: 'Enviamos um link de cadastro para seu e-mail.',
      },
    },
    FORM: {
      EMAIL: 'E-mail',
      PASSWORD: 'Senha',

    },
    BTN: {
      SIGNIN: 'ENTRAR',
      SIGNUP: 'CADASTRAR',
    },
    FOOTER: {
      LINK: 'Termos de Usos',
      link2: 'Politica de Segurança',
      link3: 'Política de Privacidade',
    },
    FORGOT_PASSWORD: {
      INFO: 'Esqueceu a senha?',
      CLICK: 'Clique aqui',
    },
  },
};
