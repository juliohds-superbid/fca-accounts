export default {
  translation: {
    FOOTER: {
      LINK: 'Terms of Use',
      link2: 'Security Policy',
      link3: 'Privacy Policy',
    },
  },
};
