export default {
  dFlex: {
    display: 'flex',
  },
  dFlexRow: {
    display: 'flex',
    flexDirection: 'row',
  },
  dFlexAlignCenterJustifyCenter: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  dFlexAlignCenterJustifyStart: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-start',
  },
  dFlexAlignCenterJustifySpaceArround: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'space-around',
  },
  dFlexAlignCenterJustifySpaceBetween: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  dFlexAlignCenterJustifyEnd: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-end',
  },
  dFlexAlignStartJustifyCenter: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'start',
  },
  dFlexColumnAlignCenterJustifyCenter: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
  },
  dFlexColumnAlignCenterJustifySpaceArround: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'space-around',
  },
  dFlexColumnAlignCenterJustifySpaceBetween: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  dFlexColumnAlignStartJustifyCenter: {
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'flex-start',
  },
  dFlexColumnAlignEndJustifyStart: {
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'flex-start',
    alignItems: 'flex-end',
  },
  dFlexColumnAlignStartJustifySpaceBetween: {
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'space-between',
    alignItems: 'flex-start',
  },
  dFlexColumnAlignEndJustifySpaceBetween: {
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'space-between',
    alignItems: 'flex-end',
  },
  dFlexColumnAlignCenterJustifyStart: {
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'flex-start',
    alignItems: 'center',
  },
};
