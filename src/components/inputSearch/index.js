import React from 'react';
import { Input, Grid } from '@material-ui/core';
import SearchIcon from '@material-ui/icons/Search';
import MakeStyles from '@material-ui/styles/makeStyles';


const useStyles = MakeStyles((theme) => ({
  container: {
    backgroundColor: theme.palette.primary.main,
  },
}));

export default function InputSearch() {
  const classes = useStyles();
  return (
    <Grid className={classes}>
      <SearchIcon />
      <Input />
    </Grid>
  );
}
