
import React from 'react';
import MakeStyles from '@material-ui/styles/makeStyles';
import LockIcon from '@material-ui/icons/Lock';
import i18n from 'i18next';
import { Link } from 'react-router-dom';
import styleUtils from '../../utils/styleUtils';

const useStyles = MakeStyles((theme) => ({
  containerRestrict: {
    ...styleUtils.dFlexAlignCenterJustifyCenter,
    position: 'absolute',
    top: 20,
    right: 30,
  },
  icon: {
    backgroundColor: theme.palette.primary.main,
    borderRadius: '50%',
    padding: 5,
  },
  text: {
    fontFamily: theme.typography.fontFamily,
    fontWeight: theme.typography.fontWeight,
    margin: 5,
  },
}));


export default function RestrictArea({ props }) {
  const classes = useStyles();

  return (
    <Link to="/admin/login">
      <div className={classes.containerRestrict}>
        <div className={classes.icon}>
          <LockIcon fontSize="small" color="secondary" />
        </div>
        <p className={classes.text}>
          {i18n.t('LOCKAREA')}
        </p>
      </div>
    </Link>
  );
}
