import React from 'react';

import MakeStyles from '@material-ui/styles/makeStyles';
import Config from '../../config/structure.config.json';
import styleUtils from '../../utils/styleUtils';

const useStyles = MakeStyles((theme) => ({
  container: {
    height: Config.admin.pageTitleHeight,
    ...styleUtils.dFlexAlignCenterJustifyStart,
  },
  text: {
    fontFamily: theme.typography.fontFamily,
    fontWeight: theme.typography.fontWeight,
    marginLeft: Config.admin.margin,
    marginRight: Config.admin.margin,
    color: theme.palette.primary.main,
    width: '100%',
    fontSize: 32,
    paddingBottom: 20,
    marginTop: 20,
    borderBottom: '1px solid #e7e7e7',
  },
}));

export default function PageTitle({ title }) {
  const classes = useStyles();

  return (
    <section className={classes.container}>
      <h1 className={classes.text}>{title}</h1>
    </section>
  );
}
