import React, { useState } from 'react';
import i18n from 'i18next';
import { makeStyles } from '@material-ui/core/styles';
import { useDispatch } from 'react-redux';
import InputIcon from '../common/inputIcon';
import ButtonBase from '../common/buttonBase';
import styleUtils from '../../utils/styleUtils';
import { loginCloseRegister } from '../../stores/login/actions';
import imgRightSendEmail from '../../assets/img/rightSendMailLogin.png';

// import { Container } from './styles';
const useStyles = makeStyles((theme) => ({
  paper: {
    backgroundColor: '#FFF',
    width: '400px',
    height: '200px',
    borderRadius: '5px',
    boxShadow: theme.shadows[1],
    padding: theme.spacing(5, 4, 3),
    ...styleUtils.dFlexColumnAlignCenterJustifySpaceBetween,
  },
  titleSend: {
    fontFamily: theme.typography.fontFamily,
    color: '#4D4D4F',
    fontSize: theme.typography.h2.fontSize,

  },
  descriptionSend: {
    fontFamily: theme.typography.fontFamily,
    color: '#646464',
    fontSize: theme.typography.h4.fontSize,

  },
  title: {
    textAlign: 'left',
    width: '100%',
    fontFamily: theme.typography.fontFamily,
    fontWeight: theme.typography.fontWeight,
    fontSize: theme.typography.h2.fontSize,
    color: '#4D4D4F',
  },
  cpf: {
    backgroundColor: '#4D4D4F',
    color: '#FFF',
    minHeight: 40,
    width: 100,
    ...styleUtils.dFlexAlignCenterJustifyCenter,
    fontFamily: theme.typography.fontFamily,
    fontWeight: theme.typography.fontWeight,
  },
  btnAction: {
    height: 60,
    width: '100%',
    ...styleUtils.dFlexAlignCenterJustifyEnd,
  },
  linkCancel: {
    color: '#4D4D4F',
    textDecoration: 'none',
    textUnderline: 'none',
    fontFamily: theme.typography.fontFamily,
    textTransform: 'uppercase',
    border: 0,
    backgroundColor: '#fff',
  },
}));

export default function SendSignup() {
  const classes = useStyles();
  const dispatch = useDispatch();
  const [step, setStep] = useState(0);

  const handleSendSigup = () => {
    setStep(1);
  };

  return (
    step === 0
      ? (
        <div className={classes.paper}>
          <h1 className={classes.title}>{i18n.t('MODAL.SIGNUP.INTRODUCTION')}</h1>
          <InputIcon
            styles={{ border: '1px solid #83898F' }}
            h={40}
            icon={() => <div className={classes.cpf}><p>CPF</p></div>}
            placeholder="583251564"
          />
          <div className={classes.btnAction}>
            <button
              type="button"
              onClick={() => dispatch(loginCloseRegister())}
              className={classes.linkCancel}
            >
              {i18n.t('CANCEL')}
            </button>
            <ButtonBase
              onClick={handleSendSigup}
              style={{ margin: 10, borderRadius: 2 }}
              color="primary"
              text={i18n.t('SEND')}
            />
          </div>
        </div>
      )
      : (
        <div className={classes.paper}>
          <img src={imgRightSendEmail} alt="success send email" title="sucess send email" />
          <h2 className={classes.titleSend}>{i18n.t('MODAL.SIGNUP.EMAIL_SEND_TITLE')}</h2>
          <p className={classes.descriptionSend}>{i18n.t('MODAL.SIGNUP.EMAIL_SEND_DESCRIPTION')}</p>
          <ButtonBase
            onClick={() => dispatch(loginCloseRegister())}
            style={{
              margin: 10, borderRadius: 2, borderColor: '#5A86DD', color: '#5A86DD',
            }}
            color="SECONDARY"
            text={i18n.t('CLOSE')}
          />
        </div>
      )
  );
}
