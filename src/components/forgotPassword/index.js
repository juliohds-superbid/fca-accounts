import PropTypes from 'prop-types';
import React from 'react';
import MakeStyles from '@material-ui/styles/makeStyles';
import i18n from 'i18next';
import { useDispatch } from 'react-redux';
import { loginSetState } from '../../stores/login/actions';
import enumStateLogin from '../../stores/login/utilsStoreLogin';

const useStyles = MakeStyles((theme) => ({
  text: {
    color: theme.palette.secondary.contrastText,
    margin: 5,
    fontSize: theme.typography.caption.fontSize,
    fontFamily: theme.typography.fontFamily,
  },
  textClick: {
    backgroundColor: '#FFF',
    border: 0,
    marginLeft: 5,
    color: theme.palette.primary.main,
    fontSize: theme.typography.caption.fontSize,
    fontFamily: theme.typography.fontFamily,
  },
}));

export default function ForgotPassword({ withMarginTop }) {
  const classes = useStyles();
  const dispatch = useDispatch();
  const style = {
    marginTop: withMarginTop,
  };

  return (
    <>
      <p style={style} className={classes.text}>
        {i18n.t('FORGOT_PASSWORD.INFO')}
        <button
          type="button"
          className={classes.textClick}
          onClick={() => dispatch(loginSetState(enumStateLogin.FORGOT_PASSWORD_EMAIL))}
          href="#"
        >
          {i18n.t('FORGOT_PASSWORD.CLICK')}
        </button>
      </p>
    </>
  );
}

ForgotPassword.propTypes = {
  withMarginTop: PropTypes.number,
};

ForgotPassword.defaultProps = {
  withMarginTop: 0,
};
