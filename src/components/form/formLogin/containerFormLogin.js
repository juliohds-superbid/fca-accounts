import PropTypes from 'prop-types';
import React, { useEffect, useState } from 'react';
import MakeStyles from '@material-ui/styles/makeStyles';
import i18n from 'i18next';
import { useSelector } from 'react-redux';
import FormLogin from './index';
import ContainerBase from '../../common/containerBase';
import logoFCA from '../../../assets/img/logo.png';
import ForgotPassword from '../../forgotPassword';
import enumStateLogin from '../../../stores/login/utilsStoreLogin';
import Backspace from '../../common/backspace';
import FormForgotPassword from '../formForgotPassword';

const useStyles = MakeStyles((theme) => ({
  wellcome: {
    marginTop: 30,
    width: '420px',
    fontWeight: theme.typography.fontWeight,
    color: '#4D4D4F',
    fontSize: theme.typography.h1.fontSize,
    fontFamily: theme.typography.fontFamily,
  },
  main: {
    fontFamily: theme.typography.fontFamily,
    marginTop: 25,
    width: '100%',
    color: '#83898F',
  },
  imgLogo: {
    marginLeft: -47,
    height: 100,
  },
  container: {
    width: '55%',
    minHeight: '100%',
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
    boxSizing: 'content-box',
  },
  content: {
    padding: '20px 10px 10px 100px', marginTop: 100, alignItems: 'flex-start',
  },
  containerForm: {
    marginTop: 30, width: '80%', minHeight: 140,
  },
}));

export default function ContainerFormLogin({ openRegister }) {
  const classes = useStyles();
  const { stateLogin } = useSelector((state) => state.login);
  const [body, setBody] = useState(<FormLogin openRegister={openRegister} />);
  const [text, setText] = useState({ main: `${i18n.t('SIGNIN.WELLCOME')}`, description: `${i18n.t('SIGNIN.MAIN')}` });

  useEffect(() => {
    if (stateLogin === enumStateLogin.INITIAL) {
      setBody(<FormLogin openRegister={openRegister} />);
      setText({ main: `${i18n.t('SIGNIN.WELLCOME_MAIN')}`, description: `${i18n.t('SIGNIN.WELLCOME_DESCRIPTION')}` });
    } if (stateLogin === enumStateLogin.FORGOT_PASSWORD_EMAIL) {
      setBody(<FormForgotPassword />);
      setText({ main: `${i18n.t('SIGNIN.FORGOT_PASSWORD_MAIN')}`, description: `${i18n.t('SIGNIN.FORGOT_PASSWORD_DESCRIPTION')}` });
    } if (stateLogin === enumStateLogin.FORGOT_PASSWORD_SUCCESS) {
      setBody(null);
      setText({ main: `${i18n.t('SIGNIN.FORGOT_SUCCESS_MAIN')}`, description: `${i18n.t('SIGNIN.FORGOT_SUCCESS_DESCRIPTION')}` });
    }
  }, [stateLogin]);

  return (
    <ContainerBase className={classes.container}>
      <ContainerBase
        column
        className={classes.content}
      >
        <img className={classes.imgLogo} src={logoFCA} title="logo" alt="logo" />
        <h3 className={classes.wellcome}>{text.main}</h3>
        <p className={classes.main}>
          {text.description}
        </p>
        <ContainerBase
          className={classes.containerForm}
          column
        >
          {body}
        </ContainerBase>

        {stateLogin === 0 ? <ForgotPassword withMarginTop={30} /> : <Backspace login to={stateLogin === 2 ? 0 : null} />}
      </ContainerBase>
    </ContainerBase>
  );
}

ContainerFormLogin.propTypes = {
  openRegister: PropTypes.func.isRequired,
};
