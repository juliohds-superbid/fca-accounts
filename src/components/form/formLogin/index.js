import PropTypes from 'prop-types';
import React from 'react';
// import { Container } from './styles';
import PersonIcon from '@material-ui/icons/Person';
import LockIcon from '@material-ui/icons/Lock';
import i18n from 'i18next';
import { Link } from 'react-router-dom';
import InputIcon from '../../common/inputIcon';
import ButtonBase from '../../common/buttonBase';
import ContainerBase from '../../common/containerBase';

export default function FormLogin({ openRegister, admin }) {
  const btnContainerStyle = { justifyContent: admin ? 'center' : 'flex-start', marginTop: 30, width: '100%' };

  return (
    <>
      <InputIcon
        icon={() => (<PersonIcon color="primary" />)}
        color="secondary"
        placeholder={i18n.t('FORM.EMAIL')}
      />
      <InputIcon
        icon={() => (<LockIcon color="primary" />)}
        color="primary"
        placeholder={i18n.t('FORM.PASSWORD')}
      />
      <ContainerBase styles={btnContainerStyle}>
        <Link style={{ textDecoration: 'none' }} to={!admin ? '/admin/login' : '/admin/register'}><ButtonBase style={{ marginRight: 10 }} color="primary" text={i18n.t('BTN.SIGNIN')} /></Link>
        {!admin && <ButtonBase color="secondary" onClick={openRegister} text={i18n.t('BTN.SIGNUP')} />}
      </ContainerBase>
    </>
  );
}

FormLogin.defaultProps = {
  admin: false,
  openRegister: null,
};

FormLogin.propTypes = {
  openRegister: PropTypes.func,
  admin: PropTypes.bool,
};
