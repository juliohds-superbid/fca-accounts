import React from 'react';
import MakeStyles from '@material-ui/styles/makeStyles';
import FormLogin from '../formLogin';
import styleUtils from '../../../utils/styleUtils';
import ContainerBase from '../../common/containerBase';
import ForgotPassword from '../../forgotPassword';

const useStyles = MakeStyles((theme) => ({
  containerFormLoginAdmin: {
    height: 360,
    padding: '65px 110px',
    backgroundColor: '#FFF',
    border: '1px solid #E5E5E5',
    borderRadius: 11,
  },
  title: {
    color: theme.palette.primary.main,
    fontSize: 32,
    fontWeight: theme.typography.fontWeight,
    fontFamily: theme.typography.fontFamily,
    width: '100%',
    textAlign: 'center',
    marginBottom: 50,
  },
  content: { minHeight: 372, ...styleUtils.dFlexColumnAlignCenterJustifyCenter },
}));

export default function FormLoginAdmin() {
  const classes = useStyles();

  return (
    <div className={classes.containerFormLoginAdmin}>
      <div className={classes.content}>
        <h1 className={classes.title}>Área restrita administrativo</h1>
        <ContainerBase column styles={{ width: '90%' }}>
          <FormLogin admin />
        </ContainerBase>
        <ForgotPassword withMarginTop={50} />
      </div>
    </div>
  );
}
