import PropTypes from 'prop-types';
import React from 'react';
import MakeStyles from '@material-ui/styles/makeStyles';
import { Link } from 'react-router-dom';
import imgLogo from '../../../assets/img/logo.png';
import Config from '../../../config/structure.config.json';
import styleUtils from '../../../utils/styleUtils';
import FormLoginAdmin from '.';

const useStyles = MakeStyles(() => ({
  imgAdmin: {
    width: 200,
    marginTop: 50,
    marginBottom: 40,
  },
  containerLoginAdmin: {
    minHeight: `calc(100% - ${Config.header.headerLoginHeight}px)`,
    ...styleUtils.dFlexColumnAlignCenterJustifyStart,
    backgroundColor: '#F8F8F8',
  },
}));

export default function ContainerFormLoginAdmin({ logo }) {
  const classes = useStyles();
  return (
    <div className={classes.containerLoginAdmin}>
      {logo && <Link to="/"><img className={classes.imgAdmin} src={imgLogo} title="" alt="" /></Link>}
      <FormLoginAdmin />
    </div>
  );
}

ContainerFormLoginAdmin.defaultProps = {
  logo: true,
};

ContainerFormLoginAdmin.propTypes = {
  logo: PropTypes.bool,
};
