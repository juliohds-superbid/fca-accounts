import React from 'react';
import i18n from 'i18next';
import { useDispatch } from 'react-redux';
import PersonIcon from '@material-ui/icons/Person';
import MakeStyles from '@material-ui/styles/makeStyles';
import InputIcon from '../../common/inputIcon';
import ButtonBase from '../../common/buttonBase';
import styleUtils from '../../../utils/styleUtils';
import { loginSetState } from '../../../stores/login/actions';
import enumStateLogin from '../../../stores/login/utilsStoreLogin';

const useStyles = MakeStyles(() => ({
  btnSend: {
    marginTop: 20,
  },
  containerBtn: {
    width: '100%',
    ...styleUtils.dFlexAlignCenterJustifyStart,
  },
}));

export default function FormForgotPassword() {
  const classes = useStyles();
  const dispatch = useDispatch();
  const nextStep = () => {
    dispatch(loginSetState(enumStateLogin.FORGOT_PASSWORD_SUCCESS));
  };
  return (
    <>
      <InputIcon icon={() => (<PersonIcon color="primary" />)} placeholder={i18n.t('FORM.EMAIL')} />
      <div className={classes.containerBtn}>
        <ButtonBase className={classes.btnSend} onClick={nextStep} color="primary" text={i18n.t('SEND')} />
      </div>
    </>
  );
}
