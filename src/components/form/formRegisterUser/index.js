import React from 'react';
import MakeStyles from '@material-ui/styles/makeStyles';

import MyInputBase from '../../common/inputBase';
import styleUtils from '../../../utils/styleUtils';
import ButtonBase from '../../common/buttonBase';

const useStyles = MakeStyles(() => ({
  containerRegisterUser: {
    marginTop: 20,
    ...styleUtils.dFlexAlignCenterJustifyCenter,
  },
  contentLeft: {
    width: '50%',
    padding: '50px 100px 50px 0px',
    minHeight: 450,
    ...styleUtils.dFlexColumnAlignCenterJustifyStart,
  },
  contentRight: {
    width: '50%',
    minHeight: 450,
    ...styleUtils.dFlexColumnAlignEndJustifySpaceBetween,
  },
}));
export default function FormRegisterUser() {
  const classes = useStyles();
  return (
    <div className={classes.containerRegisterUser}>
      <div className={classes.contentLeft}>
        <MyInputBase color="primary" text="Nome" placeholder="Nome" />
        <MyInputBase color="primary" text="Matrícula" placeholder="Matrícula" />
        <MyInputBase color="primary" text="CPF / CNPJ" placeholder="CPF / CNPJ" />
        <MyInputBase color="primary" text="RG" placeholder="RG" />
        <MyInputBase color="primary" text="CEP" placeholder="CEP" />
        <MyInputBase color="primary" text="Endereço" placeholder="Endereço" />
        <MyInputBase color="primary" text="Bairro" placeholder="Bairro" />
        <MyInputBase color="primary" text="Cidade" placeholder="Cidade" />
        <MyInputBase color="primary" text="UF" placeholder="UF" />
      </div>
      <div className={classes.contentRight}>
        <div>
          <MyInputBase color="primary" text="Telefone" placeholder="Telefone" />
          <MyInputBase color="primary" text="E-mail" placeholder="E-mail" />
          <MyInputBase color="primary" text="Confirmação de E-mail" placeholder="Confirmação de E-mail" />
          <MyInputBase color="primary" text="Data de Nascimento" placeholder="Data de Nascimento" />
          <MyInputBase color="primary" text="Tipo de Acesso" placeholder="Tipo de Acesso" />
        </div>
        <ButtonBase color="primary" text="ENVIAR" />
      </div>
    </div>
  );
}
