import React from 'react';
import MakeStyles from '@material-ui/styles/makeStyles';
import FormRegisterUser from './index';
import Config from '../../../config/structure.config.json';

const useStyles = MakeStyles((theme) => ({
  containerFormSingle: {
    padding: `20px ${Config.admin.margin}px`,
  },
  singleText: {
    fontSize: theme.typography.h1.fontSize,
    fontWeight: theme.typography.fontWeight,
    color: theme.palette.secondary.contratText,
    fontFamily: theme.typography.fontFamily,
  },
}));

export default function ContainerFormSingleRegister() {
  const classes = useStyles();

  return (
    <div className={classes.containerFormSingle}>
      <h1 className={classes.singleText}>Cadastro Único</h1>
      <FormRegisterUser />
    </div>
  );
}
