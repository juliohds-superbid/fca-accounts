import React from 'react';
import MakeStyles from '@material-ui/styles/makeStyles';
import FormAlotRegisterUser from './formAlotRegister';
import Config from '../../../config/structure.config.json';

const useStyles = MakeStyles((theme) => ({
  container: {
    paddingLeft: Config.admin.margin,
    paddingRight: Config.admin.margin,
    paddingTop: 50,
    paddingBottom: 10,
    backgroundColor: '#f8f8f8',
  },
  alotRegisterText: {
    fontSize: theme.typography.h1.fontSize,
    fontWeight: theme.typography.fontWeight,
    marginBottom: 50,
    color: theme.palette.secondary.contratText,
    fontFamily: theme.typography.fontFamily,
  },
}));
export default function ContainerFormAlotRegisterUser() {
  const classes = useStyles();

  return (
    <div className={classes.container}>
      <h1 className={classes.alotRegisterText}>Cadastro em Massa</h1>
      <FormAlotRegisterUser />
    </div>
  );
}
