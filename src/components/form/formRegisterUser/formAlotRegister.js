import React from 'react';
import MakeStyles from '@material-ui/styles/makeStyles';
import CloudDownloadIcon from '@material-ui/icons/CloudDownload';

const useStyles = MakeStyles(() => ({
  containerAlot: {
    height: 300,
  },
  upload: {
    backgroundColor: '#FFF',
    border: '2px dashed #ccc',
    height: '200px',
    cursor: 'pointer',
    padding: '6px 12px',
    width: '400px',
    borderRadius: '10px',
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
  },
}));
export default function FormAlotRegisterUser() {
  const classes = useStyles();
  return (
    <div className={classes.containerAlot}>
      <label htmlFor="file-upload" className={classes.upload}>
        <CloudDownloadIcon fontSize="large" />
        Faça o upload do seu arquivo excel
        <input id="file-upload" name="file-upload" type="file" style={{ display: 'none' }} />
      </label>
    </div>
  );
}
