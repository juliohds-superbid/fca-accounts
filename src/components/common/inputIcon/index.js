import React from 'react';
import Input from '@material-ui/core/Input';
import { useSelector } from 'react-redux';
import PropTypes from 'prop-types';
import MakeStyles from '@material-ui/styles/makeStyles';
import ContainerBase from '../containerBase';

const useStyles = MakeStyles((theme) => ({
  input: {
    '&:hover': {
      borderBottom: '0px solid',
    },
  },
  iconContainer: {
    marginRight: 10,
  },
  underline: {
    borderBottom: '0px solid',
    '&:before': {
      borderBottom: '0px solid',
      borderBottomColor: theme.palette.primary.main,
    },
    '&:after': {
      borderBottomColor: theme.palette.primary.main,
    },
    '&:hover:not($disabled):before': {
      borderBottom: '0px solid',
      height: 1,
    },
  },
  defaultInput: {
    width: '100%',
    height: 50,
  },

}));

function InputIcon({ icon, ...props }) {
  const classes = useStyles();
  const palette = useSelector((state) => state.styleConfig.data.palette);
  const colorBorder = palette ? palette.primary.main : '';
  const { w, h, styles } = props;
  return (
    <>
      <ContainerBase
        styles={{
          width: `${w || 100}%`,
          height: `${h || ''}px`,
          borderBottom: `1px solid ${colorBorder}`,
          ...styles,
        }}
      >
        <div className={classes.iconContainer}>
          {icon && icon()}
        </div>
        <Input
          disableUnderline
          classes={{
            underline: classes.underline,
            input: classes.input,
          }}
          className={classes.defaultInput}
          {...props}
        />
      </ContainerBase>
    </>
  );
}

InputIcon.propTypes = {
  icon: PropTypes.node.isRequired,
  w: PropTypes.string.isRequired,
  h: PropTypes.string.isRequired,
  props: PropTypes.objectOf.isRequired,
  styles: PropTypes.objectOf.isRequired,
};

export default InputIcon;
