import PropTypes from 'prop-types';
import React, { useState, useEffect } from 'react';
import uniqueId from 'lodash/uniqueId';

import MakeStyles from '@material-ui/styles/makeStyles';
import LinksFooter from '../../../fixtures/linksFooter';
import styleUtils from '../../../utils/styleUtils';
import ContainerBase from '../containerBase';
import Config from '../../../config/structure.config.json';

const useStyles = MakeStyles((theme) => (
  {
    container: {
      background: theme.palette.primary.main,
      height: Config.footer.footerLoginHeight,
      ...styleUtils.dFlexAlignCenterJustifyCenter,
    },
    ul: {
      ...styleUtils.dFlexRow,
      ...styleUtils.dFlexAlignCenterJustifySpaceArround,
      textAlign: 'center',
      width: '100%',
    },
    link: {
      color: theme.palette.primary.contrastText,
      textDecoration: 'none',
      fontFamily: theme.typography.fontFamily,
      fontSize: theme.typography.caption.fontSize,
    },
  }));

export default function FooterLogin({ links }) {
  const [linksFooter, setlinksFooter] = useState(LinksFooter);
  const classes = useStyles();

  useEffect(() => {
    if (links) {
      setlinksFooter(links);
    }
  }, [links]);

  return (
    <ContainerBase color="primary" className={classes.container}>
      <ul className={classes.ul}>
        {linksFooter.map((v) => (
          <li key={uniqueId(v.name)}><a className={classes.link} href={v.link}>{v.name}</a></li>))}
      </ul>
    </ContainerBase>
  );
}

FooterLogin.propTypes = {
  links: PropTypes.arrayOf.isRequired,
};
