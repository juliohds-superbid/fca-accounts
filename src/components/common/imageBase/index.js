import React from 'react';

// import { Container } from './styles';

export default function ImageBase(props) {
  // TODO COMPONENT
  const { src } = props;
  const pieces = src.split(/\//g);
  const name = pieces[pieces.length - 1];

  return (
    <img src={src} title={name} alt={name} />
  );
}
