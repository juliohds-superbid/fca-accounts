
import React from 'react';
import MakeStyles from '@material-ui/styles/makeStyles';
import styleUtils from '../../../utils/styleUtils';
import exibition from '../../../assets/img/exibitionLogin.png';

const useStyles = MakeStyles((theme) => ({

  container: {
    [theme.breakpoints.down('md')]: {
      display: 'none',
    },
    backgroundImage: `url(${exibition})`,
    backgroundPosition: 'center center',
    backgroundRepeat: 'no-repeat',
    backgroundSize: 'cover',
    height: '100%',
    width: '45%',
    ...styleUtils.dFlexAlignCenterJustifyCenter,
  },
}));

export default function ExibitionLogin() {
  const classes = useStyles();

  return (
    <div className={classes.container} />
  );
}
