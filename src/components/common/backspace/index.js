import PropTypes from 'prop-types';
import React from 'react';
import { useDispatch } from 'react-redux';
import MakeStyles from '@material-ui/styles/makeStyles';
import i18n from 'i18next';
import KeyboardBackspaceIcon from '@material-ui/icons/KeyboardBackspace';
import { loginBackState } from '../../../stores/login/actions';
import styleUtils from '../../../utils/styleUtils';

const useStyles = MakeStyles((theme) => ({
  backButton: {
    border: 0,
    backgroundColor: '#FFF',
    color: theme.palette.secondary.contrastText,
    fontSize: theme.typography.caption.fontSize,
    ...styleUtils.dFlexAlignCenterJustifyCenter,
  },
}));
export default function Backspace({ login, to }) {
  const dispatch = useDispatch();
  const classes = useStyles();
  const back = () => login && dispatch(loginBackState(to));

  return (
    <div>
      <button className={classes.backButton} type="button" onClick={back}>
        <KeyboardBackspaceIcon fontSize="small" />
        {'  '}
        {i18n.t('BACK')}
      </button>
    </div>
  );
}

Backspace.defaultProps = {
  login: false,
  to: null,
};


Backspace.propTypes = {
  login: PropTypes.bool,
  to: PropTypes.number,
};
