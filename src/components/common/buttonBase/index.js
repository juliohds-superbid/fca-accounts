import PropTypes from 'prop-types';
import React from 'react';
import Button from '@material-ui/core/Button';
import MakeStyles from '@material-ui/styles/makeStyles';

const useStyles = MakeStyles((theme) => ({
  primary: {
    color: theme.palette.primary.contrastText,
    backgroundColor: theme.palette.primary.main,
  },
  secondary: {
    color: theme.palette.secondary.contrastText,
    backgroundColor: theme.palette.secondary.main,
    border: `1px solid ${theme.palette.secondary.contrastText}`,
  },
  btn: {
    borderRadius: 5,
    padding: '5px 20px',
    fontFamily: theme.typography.fontFamily,
    fontWeight: theme.typography.fontWeight,
  },
}));

export default function ButtonBase({
  style, color, text, onClick, className,
}) {
  const classes = useStyles();

  const getColor = () => (color === 'primary' ? classes.primary : classes.secondary);

  const allClassBtn = `${classes.btn} ${getColor(color)} ${className}`;

  return (
    <Button onClick={onClick} style={style} className={allClassBtn} color={color}>{text || 'BUTTON'}</Button>
  );
}

ButtonBase.propTypes = {
  color: PropTypes.string.isRequired,
  style: PropTypes.objectOf.isRequired,
  text: PropTypes.string.isRequired,
  className: PropTypes.string.isRequired,
  onClick: PropTypes.func.isRequired,
};
