import React from 'react';
import Input from '@material-ui/core/Input';
import MakeStyles from '@material-ui/styles/makeStyles';

const useStyles = MakeStyles((theme) => ({
  input: {},
  underline: {
    '&:before': {
      borderBottom: 'thin solid',
      borderBottomColor: theme.palette.primary.main,
    },
    '&:after': {
      borderBottomColor: theme.palette.primary.main,
    },
  },
  defaultInput: {
    width: '100%',
    height: 50,
  },

}));

function MyInputBase({ ...props }) {
  const classes = useStyles();

  return (
    <>
      <Input
        classes={{
          underline: classes.underline,
          input: classes.input,
        }}
        className={classes.defaultInput}
        {...props}
      />
    </>
  );
}

export default MyInputBase;
