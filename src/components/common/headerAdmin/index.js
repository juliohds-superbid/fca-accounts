import React, { useState } from 'react';
import PropTypes from 'prop-types';
import MakeStyles from '@material-ui/styles/makeStyles';
import { Link } from 'react-router-dom';
import Config from '../../../config/structure.config.json';
import LinksHeaderAdmin from '../../../fixtures/linksHeaderAdmin';
import styleUtils from '../../../utils/styleUtils';

const useStyles = MakeStyles((theme) => ({
  container: {
    backgroundColor: theme.palette.primary.main,
    height: Config.header.headerLoginHeight,
    ...styleUtils.dFlexAlignCenterJustifyCenter,
  },
  containerLink: {
    ...styleUtils.dFlexAlignCenterJustifyStart,
    width: '80%',
    height: '100%',
    marginLeft: Config.admin.margin - 30,
  },
  contentLink: {
    padding: '8px 30px 8px 30px',
    marginRight: 5,
    color: '#FFF',
    fontFamily: theme.typography.fontFamily,
    fontSize: theme.typography.h4.fontSize,
  },
  link: {
    textDecoration: 'none',
    color: '#FFF',
    fontFamily: theme.typography.fontFamily,
  },
  borderDetail: {
    borderRight: `1px solid ${theme.palette.primary.contrastText} `,
  },
}));

export default function HeaderAdmin({ isLogged, exit }) {
  const [links] = useState(LinksHeaderAdmin);
  const classes = useStyles();
  return (
    <div className={classes.container}>
      {isLogged
      && (
      <ul className={classes.containerLink}>
        {links.map((v, i) => <li key={`header-admin-${v.name.trim()}`} className={`${classes.contentLink} ${i !== links.length - 1 ? classes.borderDetail : ''}`}><Link className={classes.link} to={v.link}>{v.name}</Link></li>)}
      </ul>
      )}
      {exit && <Link to="/">Sair</Link>}
    </div>
  );
}

HeaderAdmin.defaultProps = {
  exit: false,
  isLogged: false,
};

HeaderAdmin.propTypes = {
  exit: PropTypes.bool,
  isLogged: PropTypes.bool,
};
