import React from 'react';
import MakeStyles from '@material-ui/styles/makeStyles';
import Config from '../../../config/structure.config.json';

import styleUtils from '../../../utils/styleUtils';

const useStyles = MakeStyles((theme) => (
  {
    container: {
      ...styleUtils.dFlexAlignCenterJustifyCenter,
    },
  }));

export default function ContainerBase(props) {
  const classes = useStyles();
  const {
    withFooter, styles, column, className,
  } = props;

  const componentStyles = {
    flexDirection: column ? 'column' : 'row',
    ...styles,
  };

  if (withFooter) {
    componentStyles.height = `calc(100% - ${Config.footer.footerLoginHeight}px)`;
  }

  return (
    <div
      className={classes.container.concat(' ', className)}
      style={componentStyles}
    >
      {props.children}
    </div>
  );
}
