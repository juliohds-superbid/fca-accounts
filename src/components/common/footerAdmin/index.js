import React, { useState } from 'react';
import MakeStyles from '@material-ui/styles/makeStyles';
import { Typography } from '@material-ui/core';
import Config from '../../../config/structure.config.json';
import LinksFooterAdmin from '../../../fixtures/linksFooterAdmin';
import styleUtils from '../../../utils/styleUtils';
import imgLogo from '../../../assets/img/logo.png';
import imgLogoAdminFooter from '../../../assets/img/logoAdminFooter.png';

const useStyles = MakeStyles((theme) => ({
  container: {
    minHeight: 240,
    backgroundColor: '#FFF',
    ...styleUtils.dFlexAlignCenterJustifyCenter,
    paddingLeft: Config.admin.margin,
    paddingRight: Config.admin.margin,
  },
  title: {
    fontFamily: theme.typography.fontFamily,
    fontSize: theme.typography.h2.fontSize,
    fontWeight: theme.typography.fontWeight,
  },
  contentLeft: {
    width: '40%',
    ...styleUtils.dFlexColumnAlignStartJustifyCenter,
  },
  contentRight: {
    width: '60%',
    minHeight: 144,
    ...styleUtils.dFlexColumnAlignStartJustifySpaceBetween,
  },
  imgLogo: {
    width: 100,
    marginLeft: -20,
    marginBottom: 30,
  },
  imgLogoFooter: {
    width: 100,
    marginLeft: -20,
  },
  link: {
    padding: '8px 30px 8px 0px',
    marginRight: 5,
    fontFamily: theme.typography.fontFamily,
    fontSize: theme.typography.h4.fontSize,
  },
  borderDetail: {
    borderRight: `1px solid ${theme.palette.primary.contrastText} `,
  },
  containerFooter: {
    backgroundColor: '#f8f8f8',
    marginBottom: 20,
    paddingBottom: 15,
    paddingTop: 15,
    ...styleUtils.dFlexAlignCenterJustifySpaceArround,

  },
  linksSectionFooter: {
    ...styleUtils.dFlexAlignCenterJustifyCenter,
  },
  linksFooter: {
    fontSize: theme.typography.h6.fontSize,
    padding: '5px 30px 5px 30px',
    '&:not(:last-child)': {
      borderRight: '1px solid #000',
    },
  },
}));

export default function FooterAdmin() {
  const [links] = useState(LinksFooterAdmin);
  const classes = useStyles();
  return (
    <footer>
      <section className={classes.container}>
        <div className={classes.contentLeft}>
          <img src={imgLogo} className={classes.imgLogo} alt="logo" title="logo" />
          <ul className={classes.containerLink}>
            {links.map((v, i) => <li key={`footer-admin-${v.name.trim()}`} className={`${classes.link} ${i !== links.length - 1 ? classes.borderDetail : ''}`}>{v.name}</li>)}
          </ul>
        </div>
        <div className={classes.contentRight}>
          <div>
            <h1 className={classes.title}>Contato FCA</h1>
            <Typography>Segunda a sexta das 9h às 18h</Typography>
          </div>
          <Typography>Para contatos direto, use um dos nossos canais abaixo:</Typography>
          <div>
            <Typography>Telefone: (11) 1234-5678</Typography>
            <Typography>E-mail: atendimento@empresa.com.br</Typography>
          </div>
        </div>
      </section>
      <section className={classes.containerFooter}>
        <img src={imgLogoAdminFooter} className={classes.imgLogoFooter} alt="logo" title="logo" />
        <ul className={classes.linksSectionFooter}>
          <li className={classes.linksFooter}>Politica e Privacidade</li>
          <li className={classes.linksFooter}>Politica e Segurança</li>
          <li className={classes.linksFooter}>Termos de Uso</li>
          <li className={classes.linksFooter}>Suporte Tecnico</li>
        </ul>
      </section>
    </footer>
  );
}
