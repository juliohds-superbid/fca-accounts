import React from 'react';
import { Switch } from 'react-router-dom';

import Route from './routes';
import LoginScreen from './screens/loginScreen';
import LoginAdminScreen from './screens/loginAdminScreen';
import AdminRegisterUserScreen from './screens/AdminRegisterUserScreen';
import AdminLoadUserScreen from './screens/AdminLoadUserScreen';

const AllRoutes = () => {
  const routes = React.useMemo(
    () => (
      <Switch>
        <Route exact fullwidth path="/" component={LoginScreen} />
        <Route exact fullwidth path="/admin/login" component={LoginAdminScreen} />
        <Route exact fullwidth path="/admin/list" component={AdminLoadUserScreen} />
        <Route exact fullwidth path="/admin/register" component={AdminRegisterUserScreen} />
        <Route component={LoginScreen} />
      </Switch>
    ),
  );

  return (
    <>
      {routes}
    </>
  );
};

export default AllRoutes;
