import React from 'react';
import Helmet from 'react-helmet';
import { useSelector, useDispatch } from 'react-redux';
import { MuiThemeProvider } from '@material-ui/core/styles';
import { styleConfigRequest } from '../stores/styleConfig/actions';
import Config from '../config/structure.config.json';
import theme from './index';
import AllRoutes from '../allRoutes';


export default function Layout() {
  const { isLoading, data } = useSelector((state) => state.styleConfig);
  const dispatch = useDispatch();

  React.useEffect(() => {
    dispatch(styleConfigRequest());
  }, []);

  return (

    <MuiThemeProvider theme={theme({ color: { ...data.palette }, text: { ...data.typography } })}>
      <Helmet>
        <title>{Config.title}</title>
      </Helmet>
      {!isLoading && (
        <AllRoutes />
      )}
    </MuiThemeProvider>
  );
}
