import createMuiTheme from '@material-ui/core/styles/createMuiTheme';

export default (paletteColors) => createMuiTheme({
  typography: {
    fontWeight: 800,
    useNextVariants: true,
    fontSize: 14,
    h1: {
      fontSize: 24,
      fontWeight: 700,
    },
    h2: {
      fontSize: 20,
      fontWeight: 800,
    },
    h3: {
      fontSize: 16,
      fontWeight: 400,
    },
    h4: {
      fontSize: 14,
      fontWeight: 400,
      textTransform: 'uppercase',
    },
    h6: {
      fontSize: 14,
      fontWeight: 700,
      textTransform: 'uppercase',
    },
    body1: {
      fontSize: 14,
      fontWeight: 400,
    },
    body2: {
      fontSize: 14,
      fontWeight: 800,
    },
    button: {
      fontSize: 14,
      fontWeight: 800,
      letterSpacing: '1.3px',
    },
    caption: {
      fontSize: 12,
      fontWeight: 400,
    },
    ...paletteColors.text,
  },
  palette: {
    basic: {
      white: '#FFFFFF',
    },
    error: {
      main: '#D40026',
      contrastText: '#fff',
    },
    success: {
      main: '#38AC38',
      contrastText: '#fff',
    },
    waitingApproval: {
      main: '#36AF86',
    },
    ...paletteColors.color,
  },
});
