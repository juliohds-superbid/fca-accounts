import React from 'react';
import { useSelector, useDispatch } from 'react-redux';
import FooterLogin from '../../components/common/footer/footerLogin';
import ExibitionLogin from '../../components/common/exibition/exibitionLogin';
import ContainerFormLogin from '../../components/form/formLogin/containerFormLogin';
import ContainerBase from '../../components/common/containerBase';
import ModalBase from '../../components/common/modalBase';
import SendSignup from '../../components/sendSignup';
import { loginCloseRegister, loginOpenRegister } from '../../stores/login/actions';
import RestrictArea from '../../components/restrictArea';

export default function LoginScreen() {
  const dispatch = useDispatch();
  const { openRegister } = useSelector((state) => state.login);
  return (
    <>
      <ModalBase
        handleClose={() => dispatch(loginCloseRegister())}
        open={openRegister}
        component={() => <SendSignup />}
      />
      <ContainerBase withFooter>
        <RestrictArea />
        <ExibitionLogin />
        <ContainerFormLogin openRegister={() => dispatch(loginOpenRegister())} />
      </ContainerBase>
      <FooterLogin />
    </>
  );
}
