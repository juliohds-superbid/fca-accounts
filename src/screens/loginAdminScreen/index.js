import React from 'react';
import HeaderAdmin from '../../components/common/headerAdmin';
import ContainerFormLoginAdmin from '../../components/form/formLoginAdmin/containerFormLoginAdmin';

export default function LoginAdminScreen() {
  return (
    <>
      <HeaderAdmin />
      <ContainerFormLoginAdmin logo />
    </>
  );
}
