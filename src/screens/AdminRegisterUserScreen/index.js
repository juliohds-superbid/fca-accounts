import React from 'react';
import HeaderAdmin from '../../components/common/headerAdmin';
import FooterAdmin from '../../components/common/footerAdmin';

import PageTitle from '../../components/pageTitle';
import ContainerFormSingleRegister from '../../components/form/formRegisterUser/containerFormSingleRegister';
import ContainerFormAlotRegisterUser from '../../components/form/formRegisterUser/containerFormAlotRegister';

// import { Container } from './styles';

export default function AdminRegisterUserScreen() {
  return (
    <>
      <HeaderAdmin isLogged exit />
      <PageTitle title="Cadastro de Usuário" />
      <ContainerFormSingleRegister />
      <ContainerFormAlotRegisterUser />
      <FooterAdmin />
    </>
  );
}
