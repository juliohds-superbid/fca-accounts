import React from 'react';
import HeaderAdmin from '../../components/common/headerAdmin';
import FooterAdmin from '../../components/common/footerAdmin';

import PageTitle from '../../components/pageTitle';
import ContainerListUser from '../../components/containerListUser';

// import { Container } from './styles';

export default function AdminLoadUserScreen() {
  return (
    <>
      <HeaderAdmin isLogged exit />
      <PageTitle title="Lista de usuários" />
      <ContainerListUser />
      <FooterAdmin />
    </>
  );
}
